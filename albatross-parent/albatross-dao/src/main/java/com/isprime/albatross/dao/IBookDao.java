package com.isprime.albatross.dao;

import java.util.List;

import javax.sql.DataSource;

import com.isprime.albatross.beans.BookBean;

public interface IBookDao {
	public void setDataSource(DataSource dataSource);
	public BookBean add(BookBean book);
	public BookBean update(BookBean book);
	public boolean remove(BookBean book);
	public BookBean bookById(long id);
	public List<BookBean> search(String department);
	public BookBean getLast();
}
