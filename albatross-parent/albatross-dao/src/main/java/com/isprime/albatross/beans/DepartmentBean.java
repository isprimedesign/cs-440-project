package com.isprime.albatross.beans;

import java.util.Date;

public class DepartmentBean {
	private String departmentCode;
	private String departmentDesc;
	private Date modDt;
	
	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	public String getDepartmentDesc() {
		return departmentDesc;
	}
	public void setDepartmentDesc(String departmentDesc) {
		this.departmentDesc = departmentDesc;
	}
	public Date getModDt() {
		return modDt;
	}
	public void setModDt(Date modDt) {
		this.modDt = modDt;
	}
	
	@Override
	public String toString() {
		return "DepartmentBean [departmentCode=" + departmentCode
				+ ", departmentDesc=" + departmentDesc + ", modDt=" + modDt
				+ "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((departmentCode == null) ? 0 : departmentCode.hashCode());
		result = prime * result
				+ ((departmentDesc == null) ? 0 : departmentDesc.hashCode());
		result = prime * result + ((modDt == null) ? 0 : modDt.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DepartmentBean other = (DepartmentBean) obj;
		if (departmentCode == null) {
			if (other.departmentCode != null)
				return false;
		} else if (!departmentCode.equals(other.departmentCode))
			return false;
		if (departmentDesc == null) {
			if (other.departmentDesc != null)
				return false;
		} else if (!departmentDesc.equals(other.departmentDesc))
			return false;
		if (modDt == null) {
			if (other.modDt != null)
				return false;
		} else if (!modDt.equals(other.modDt))
			return false;
		return true;
	}
	
	
}
