package com.isprime.albatross.dao;

import java.util.Date;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.isprime.albatross.beans.ContactBean;
import com.isprime.albatross.mapper.ContactRowMapper;

public class ContactDaoImpl extends JdbcDaoSupport implements IContactDao {
	private static final String ADD_CONTACT = "insert into Contact (CONTACT_NAME, CONTACT_EMAIL, CONTACT_PHONE, MOD_DT) "
			+ "values(?,?,?,?)";
	private static final String UPDATE_CONTACT = "update Contact set CONTACT_NAME=?, CONTACT_EMAIL=?, CONTACT_PHONE=?, MOD_DT=? where CONTACT_ID=?";
	private static final String REMOVE_CONTACT = "delete from Contact where CONTACT_ID=?";
	private static final String SELECT_BY_ID = "select CONTACT_ID, CONTACT_NAME, CONTACT_EMAIL, CONTACT_PHONE, MOD_DT from Contact where CONTACT_ID=?";
	private static final String SELECT_LAST = "select CONTACT_ID, CONTACT_NAME, CONTACT_EMAIL, CONTACT_PHONE, MOD_DT from Contact order by CONTACT_ID desc limit 1";

	public ContactBean add(ContactBean contact) {
		getJdbcTemplate().update(
				ADD_CONTACT,
				new Object[] { contact.getName(), contact.getEmail(),
						contact.getPhone(), new Date() });
		return contact;
	}

	public ContactBean update(ContactBean contact) {
		contact.setModDt(new Date());
		getJdbcTemplate().update(UPDATE_CONTACT, contact.getName(),
				contact.getEmail(), contact.getPhone(), contact.getModDt());

		return contact;
	}

	public boolean remove(ContactBean contact) {
		try {
			getJdbcTemplate().update(REMOVE_CONTACT,
					new Object[] { contact.getContactId() });
		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public ContactBean getById(long id) {
		return (ContactBean) getJdbcTemplate()
				.queryForObject(SELECT_BY_ID,
						new Object[] { id },
						new ContactRowMapper());
	}

	public ContactBean getLast() {
		return (ContactBean) getJdbcTemplate().queryForObject(SELECT_LAST,
				new ContactRowMapper());
	}

}
