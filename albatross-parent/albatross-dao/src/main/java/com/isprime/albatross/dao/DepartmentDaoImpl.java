package com.isprime.albatross.dao;

import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.isprime.albatross.beans.DepartmentBean;
import com.isprime.albatross.mapper.DepartmentRowMapper;

public class DepartmentDaoImpl extends JdbcDaoSupport implements IDepartmentDao {
	private static final String INSERT_DEPARTMENT = "insert into Departments (DEPARTMENT_CD, DEPARTMENT_DESCRIP_TX, MOD_DT) values(?,?,?)";
	private static final String UPDATE_DEPARTMENT = "update Departments set DEPARTMENT_CD=?, DEPARTMENT_DESCRIP_TX=?, MOD_DT=? where DEPARTMENT_DESCRIP_TX=?";
	private static final String DELETE_DEPARTMENT = "delete from Departments where DEPARTMENT_DESCRIP_TX";
	private static final String SELECT_ALL = "select DEPARTMENT_CD, DEPARTMENT_DESCRIP_TX, MOD_DT from Departments order by DEPARTMENT_DESCRIP_TX";
	private static final String SELECT_DEPARTMENT = "select DEPARTMENT_CD, DEPARTMENT_DESCRIP_TX, MOD_DT from Departments where DEPARTMENT_DESCRIP_TX=?";

	public DepartmentBean add(DepartmentBean department) {
		getJdbcTemplate()
				.update(INSERT_DEPARTMENT,
						new Object[] { department.getDepartmentCode(),
								department.getDepartmentDesc(),
								department.getModDt() });
		return department;
	}

	public DepartmentBean update(DepartmentBean department) {
		department.setModDt(new Date());
		getJdbcTemplate()
				.update(UPDATE_DEPARTMENT,
						new Object[] { department.getDepartmentCode(),
								department.getDepartmentDesc(),
								department.getModDt() });

		return department;
	}

	public boolean remove(DepartmentBean department) {
		try {
			getJdbcTemplate().update(DELETE_DEPARTMENT, new Object[]{department.getDepartmentDesc()});
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}

	public List<DepartmentBean> getAll() {
		List<DepartmentBean> result = getJdbcTemplate().query(SELECT_ALL,
				new DepartmentRowMapper());
		return result;
	}

	public List<DepartmentBean> getDepartments(String code) {
		List<DepartmentBean> result = getJdbcTemplate().query(SELECT_DEPARTMENT, new Object[]{code}, 
				new DepartmentRowMapper());
		
		return result;
	}
	
	public DepartmentBean getDepartment(String code) {
		return (DepartmentBean) getJdbcTemplate().queryForObject(SELECT_DEPARTMENT, new Object[]{code}, new DepartmentRowMapper());
	}

}
