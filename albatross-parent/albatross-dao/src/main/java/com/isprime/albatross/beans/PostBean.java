package com.isprime.albatross.beans;

public class PostBean {
	//Book info
		private long id;
		private String isbn;
		private String title;
		private String author;
		private String edition;
		private int published;
		private int condition;
		private String comment;
		private String department;
		private String price;
		
		//Contact info
		private String name;
		private String email;
		private String phone;
		
		public PostBean() {
			
		}
		
		public PostBean(String title, String author, int condition, String department, String price) {
			this.title = title;
			this.author = author;
			this.condition = condition;
			this.department = department;
			this.price = price;
		}
		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getIsbn() {
			return isbn;
		}
		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public String getEdition() {
			return edition;
		}
		public void setEdition(String edition) {
			this.edition = edition;
		}
		public int getPublished() {
			return published;
		}
		public void setPublished(int published) {
			this.published = published;
		}
		public int getCondition() {
			return condition;
		}
		public void setCondition(int condition) {
			this.condition = condition;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}
		public String getDepartment() {
			return department;
		}
		public void setDepartment(String department) {
			this.department = department;
		}
		public String getPrice() {
			return price;
		}
		public void setPrice(String price) {
			this.price = price;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		@Override
		public String toString() {
			return "Post [id=" + id + ", isbn=" + isbn + ", title=" + title
					+ ", author=" + author + ", edition=" + edition
					+ ", published=" + published + ", condition=" + condition
					+ ", comment=" + comment + ", department=" + department
					+ ", price=" + price + ", name=" + name + ", email=" + email
					+ ", phone=" + phone + "]";
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((author == null) ? 0 : author.hashCode());
			result = prime * result + ((comment == null) ? 0 : comment.hashCode());
			result = prime * result + condition;
			result = prime * result
					+ ((department == null) ? 0 : department.hashCode());
			result = prime * result + ((edition == null) ? 0 : edition.hashCode());
			result = prime * result + ((email == null) ? 0 : email.hashCode());
			result = prime * result + (int) (id ^ (id >>> 32));
			result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((phone == null) ? 0 : phone.hashCode());
			result = prime * result + ((price == null) ? 0 : price.hashCode());
			result = prime * result + published;
			result = prime * result + ((title == null) ? 0 : title.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			PostBean other = (PostBean) obj;
			if (author == null) {
				if (other.author != null)
					return false;
			} else if (!author.equals(other.author))
				return false;
			if (comment == null) {
				if (other.comment != null)
					return false;
			} else if (!comment.equals(other.comment))
				return false;
			if (condition != other.condition)
				return false;
			if (department == null) {
				if (other.department != null)
					return false;
			} else if (!department.equals(other.department))
				return false;
			if (edition == null) {
				if (other.edition != null)
					return false;
			} else if (!edition.equals(other.edition))
				return false;
			if (email == null) {
				if (other.email != null)
					return false;
			} else if (!email.equals(other.email))
				return false;
			if (id != other.id)
				return false;
			if (isbn == null) {
				if (other.isbn != null)
					return false;
			} else if (!isbn.equals(other.isbn))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (phone == null) {
				if (other.phone != null)
					return false;
			} else if (!phone.equals(other.phone))
				return false;
			if (price == null) {
				if (other.price != null)
					return false;
			} else if (!price.equals(other.price))
				return false;
			if (published != other.published)
				return false;
			if (title == null) {
				if (other.title != null)
					return false;
			} else if (!title.equals(other.title))
				return false;
			return true;
		}
}
