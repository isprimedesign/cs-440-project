package com.isprime.albatross.dao;

import java.util.List;

import com.isprime.albatross.beans.BookToDepartmentBean;

public interface IBookToDepartmentDao {
	public boolean addDepartmentToBook(BookToDepartmentBean b2d);
	public boolean removeDepartmentFromBook(BookToDepartmentBean b2d);
	public List<BookToDepartmentBean> getBooksFromDepartment(String department);
}
