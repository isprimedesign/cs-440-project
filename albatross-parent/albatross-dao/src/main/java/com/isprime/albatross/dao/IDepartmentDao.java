package com.isprime.albatross.dao;

import java.util.List;

import javax.sql.DataSource;

import com.isprime.albatross.beans.DepartmentBean;

public interface IDepartmentDao {
	public void setDataSource(DataSource dataSource);
	public DepartmentBean add(DepartmentBean department);
	public DepartmentBean update(DepartmentBean department);
	public boolean remove(DepartmentBean department);
	public List<DepartmentBean> getAll();
	public List<DepartmentBean> getDepartments(String code);
	public DepartmentBean getDepartment(String code);
}
