package com.isprime.albatross.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.isprime.albatross.beans.ContactBean;

public class ContactRowMapper implements RowMapper<ContactBean>{

	public ContactBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		ContactBean contact = new ContactBean();
		contact.setContactId(rs.getLong("CONTACT_ID"));
		contact.setName(rs.getString("CONTACT_NAME"));
		contact.setEmail(rs.getString("CONTACT_EMAIL"));
		contact.setPhone(rs.getString("CONTACT_PHONE"));
		contact.setModDt(rs.getDate("MOD_DT"));
		
		return contact;
	}

}
