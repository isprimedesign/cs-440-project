package com.isprime.albatross.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.isprime.albatross.beans.BookToDepartmentBean;

public class BookToDepartmentRowMapper implements RowMapper<BookToDepartmentBean>{

	public BookToDepartmentBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		BookToDepartmentBean b2d = new BookToDepartmentBean();
		b2d.setBookId(rs.getLong("BOOK_ID"));
		b2d.setDepartmentCode(rs.getString("DEPARTMENT_CD"));
		
		return b2d;
	}

}
