package com.isprime.albatross.dao;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.isprime.albatross.beans.BookToDepartmentBean;
import com.isprime.albatross.mapper.BookToDepartmentRowMapper;

public class BookToDepartmentDaoImpl extends JdbcDaoSupport implements IBookToDepartmentDao {
	private static final String INSERT_DEPARTMENT = "insert into Books2Departments (BOOK_ID, DEPARTMENT_CD) values(?,?)";
	private static final String REMOVE_DEPARTMENT = "delete from Books2Departments where BOOK_ID=? and DEPARTMENT_CD=?";
	private static final String SELECT_DEPARTMENT = "select BOOK_ID, DEPARTMENT_CD from Books2Departments where DEPARTMENT_CD=?";

	public boolean addDepartmentToBook(BookToDepartmentBean b2d) {
		int result = getJdbcTemplate().update(INSERT_DEPARTMENT, new Object[]{b2d.getBookId(), b2d.getDepartmentCode()});
		
		if(result > 0) {
			return true;
		}
		return false;
	}

	public boolean removeDepartmentFromBook(BookToDepartmentBean b2d) {
		int result = getJdbcTemplate().update(REMOVE_DEPARTMENT, new Object[]{b2d.getBookId(), b2d.getDepartmentCode()});
		
		if(result > 0) {
			return true;
		}
		return false;
	}

	public List<BookToDepartmentBean> getBooksFromDepartment(String department) {
		List<BookToDepartmentBean> result = getJdbcTemplate().query(SELECT_DEPARTMENT, new Object[]{department}, 
				new BookToDepartmentRowMapper());
		
		return result;
	}

}
