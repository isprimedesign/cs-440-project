package com.isprime.albatross.dao;

import javax.sql.DataSource;

import com.isprime.albatross.beans.ContactBean;

public interface IContactDao {
	public void setDataSource(DataSource dataSource);
	public ContactBean add(ContactBean contact);
	public ContactBean update(ContactBean contact);
	public boolean remove(ContactBean contac);
	public ContactBean getById(long id);
	public ContactBean getLast();
}
