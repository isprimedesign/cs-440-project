package com.isprime.albatross.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.isprime.albatross.beans.DepartmentBean;

public class DepartmentRowMapper implements RowMapper<DepartmentBean> {

	public DepartmentBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		DepartmentBean department = new DepartmentBean();
		department.setDepartmentCode(rs.getString("DEPARTMENT_CD"));
		department.setDepartmentDesc(rs.getString("DEPARTMENT_DESCRIP_TX"));
		department.setModDt(rs.getDate("MOD_DT"));
		
		return department;
	}
	
}
