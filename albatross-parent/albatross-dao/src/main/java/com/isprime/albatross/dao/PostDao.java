package com.isprime.albatross.dao;

import java.util.List;

import javax.sql.DataSource;

import com.isprime.albatross.beans.PostBean;

public interface PostDao {
	public void setDataSource(DataSource dataSource);
	public PostBean getById(PostBean book);
	public PostBean getByTitle(String title);
	public List<PostBean> list();
	public PostBean add(PostBean book);
	public PostBean update(PostBean book);
	public boolean remove(PostBean book);
}
