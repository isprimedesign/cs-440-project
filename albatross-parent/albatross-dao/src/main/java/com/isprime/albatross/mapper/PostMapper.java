package com.isprime.albatross.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.isprime.albatross.beans.PostBean;

public class PostMapper implements RowMapper<PostBean> {
	
	public PostBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		PostBean post = new PostBean();
		
		//Give a post object corresponding info from db.
		post.setId(Long.parseLong(rs.getString("id")));
		post.setIsbn(rs.getString("isbn"));
		post.setTitle(rs.getString("title"));
		post.setAuthor(rs.getString("author"));
		post.setEdition(rs.getString("edition"));
		post.setPublished(Integer.parseInt(rs.getString("published")));
		post.setCondition(Integer.parseInt(rs.getString("postCondition")));
		post.setComment(rs.getString("postComment"));
		post.setDepartment(rs.getString("department"));
		post.setPrice(rs.getString("price"));
		
		post.setName(rs.getString("contactName"));
		post.setEmail(rs.getString("contactEmail"));
		post.setPhone(rs.getString("contactPhone"));
		
		return post;
	}
}
