package com.isprime.albatross.dao;

import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.isprime.albatross.beans.PostBean;
import com.isprime.albatross.mapper.PostMapper;

public class PostDaoImpl extends JdbcDaoSupport implements PostDao {
	private static final String SELECT_ALL = "select * from post";
	private static final String SELECT_BY_ID = "select * from post where id=?";
	private static final String SELECT_BY_TITLE = "select * from post where title=?";
	private static final String INSERT_POST = "insert into post (isbn, title, author, edition, published, postCondition, postComment, department, price, contactName, contactEmail, contactPhone) "
			+ "values(?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String UPDATE_POST = "update post set isbn=?, title=?, author=?, edition=?, published=?, bookCondition=?, postComment=?, department=?, price=?, contactName=?, contactEmail=?, contactPhone=? where id=?";
	private static final String DELETE_POST = "delete from post where id=?";

	public PostBean getById(PostBean postBean) {
		return getJdbcTemplate().queryForObject(SELECT_BY_ID,
				new Object[] { postBean.getId() }, new PostMapper());
	}

	public PostBean getByTitle(String title) {
		return getJdbcTemplate().queryForObject(SELECT_BY_TITLE,
				new Object[] { title }, new PostMapper());
	}

	public List<PostBean> list() {
		List<PostBean> postBeanList = getJdbcTemplate().query(SELECT_ALL,
				new PostMapper());
		return postBeanList;
	}

	public PostBean add(PostBean postBean) {
		getJdbcTemplate().update(
				INSERT_POST,
				new Object[] { postBean.getIsbn(), postBean.getTitle(),
						postBean.getAuthor(), postBean.getEdition(),
						postBean.getPublished(), postBean.getCondition(),
						postBean.getComment(), postBean.getDepartment(),
						postBean.getPrice(), postBean.getName(), postBean.getEmail(),
						postBean.getPhone() });
		return postBean;
	}

	public PostBean update(PostBean postBean) {
		PostBean updatePostBean = getById(postBean);

		if (!postBean.getIsbn().equals("")) {
			updatePostBean.setIsbn(postBean.getIsbn());
		}
		if (!postBean.getTitle().equals("")) {
			updatePostBean.setTitle(postBean.getTitle());
		}
		if (!postBean.getAuthor().equals("")) {
			updatePostBean.setTitle(postBean.getAuthor());
		}
		if (postBean.getCondition() != 0) {
			updatePostBean.setCondition(postBean.getCondition());
		}
		if (!postBean.getComment().equals("")) {
			updatePostBean.setComment(postBean.getComment());
		}
		if (!postBean.getDepartment().equals("")) {
			updatePostBean.setDepartment(postBean.getDepartment());
		}
		if (!postBean.getPrice().equals("")) {
			updatePostBean.setPrice(postBean.getPrice());
		}
		if (!postBean.getName().equals("")) {
			updatePostBean.setName(postBean.getName());
		}
		if (!postBean.getEmail().equals("")) {
			updatePostBean.setEmail(postBean.getEmail());
		}
		if (!postBean.getPhone().equals("")) {
			updatePostBean.setPhone(postBean.getPhone());
		}
		
		getJdbcTemplate().update(UPDATE_POST,
				new Object[] { updatePostBean.getIsbn(), updatePostBean.getTitle(),
				updatePostBean.getAuthor(), updatePostBean.getEdition(),
				updatePostBean.getPublished(), updatePostBean.getCondition(),
				updatePostBean.getComment(), updatePostBean.getDepartment(),
				updatePostBean.getPrice(), updatePostBean.getName(), updatePostBean.getEmail(),
				updatePostBean.getPhone() });

		return updatePostBean;
	}

	public boolean remove(PostBean postBean) {
		try {
			getJdbcTemplate().update(DELETE_POST, new Object[]{postBean.getId()});
		}
		catch(Exception e) {
			return false;
		}
		return true;
	}
}
