package com.isprime.albatross.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.isprime.albatross.beans.BookBean;

public class BookRowMapper implements RowMapper<BookBean> {

	public BookBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		BookBean book = new BookBean();
		book.setBookId(rs.getLong("BOOK_ID"));
		book.setIsbn10(rs.getString("ISBN_10"));
		book.setIsbn13(rs.getString("ISBN_13"));
		book.setTitle(rs.getString("TITLE"));
		book.setAuthor(rs.getString("AUTHOR"));
		book.setEdition(rs.getString("EDITION"));
		book.setYearPublished(rs.getString("YEAR_PUBLISHED"));
		book.setCondition(rs.getString("BOOK_CONDITION"));
		book.setPrice(rs.getString("PRICE"));
		book.setComments(rs.getString("COMMENTS"));
		book.setModDt(rs.getDate("MOD_DT"));
		book.setContactId(rs.getInt("CONTACT_ID"));
		
		return book;
	}
	
}
