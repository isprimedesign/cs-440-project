package com.isprime.albatross.dao;

import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.isprime.albatross.beans.BookBean;
import com.isprime.albatross.mapper.BookRowMapper;

public class BookDaoImpl extends JdbcDaoSupport implements IBookDao {
	private static final String INSERT_BOOK = "insert into Books (ISBN_10, ISBN_13, TITLE, AUTHOR, EDITION, YEAR_PUBLISHED, BOOK_CONDITION, PRICE, COMMENTS, MOD_DT, CONTACT_ID) "
			+ "values(?,?,?,?,?,?,?,?,?,?,?)";
	private static final String UPDATE_BOOK = "update Books set ISBN_10=?, ISBN_13=?, TITLE=?, AUTHOR=?, EDITION=?, YEAR_PUBLISHED=?, BOOK_CONDITION=?, PRICE=?, COMMENTS=?, MOD_DT=?, CONTACT_ID=? where BOOK_ID=?";
	private static final String REMOVE_BOOK = "delete from Books where BOOK_ID=?";
	private static final String SELECT_BY_ID = "select BOOK_ID, ISBN_10, ISBN_13, TITLE, AUTHOR, EDITION, YEAR_PUBLISHED, BOOK_CONDITION, PRICE, COMMENTS, MOD_DT, CONTACT_ID from Books where BOOK_ID=?";
	private static final String SELECT_BY_SEARCH = "select BOOK_ID, ISBN_10, ISBN_13, TITLE, AUTHOR, EDITION, YEAR_PUBLISHED, BOOK_CONDITION, PRICE, COMMENTS, MOD_DT, CONTACT_ID from Books where upper(TITLE)=upper(?)";
	private static final String SELECT_LAST = "select BOOK_ID, ISBN_10, ISBN_13, TITLE, AUTHOR, EDITION, YEAR_PUBLISHED, BOOK_CONDITION, PRICE, COMMENTS, MOD_DT, CONTACT_ID from Books order by BOOK_ID desc limit 1";

	public BookBean add(BookBean book) {
		getJdbcTemplate().update(
				INSERT_BOOK,
				new Object[] {book.getIsbn10(),
						book.getIsbn13(), book.getTitle(), book.getAuthor(),
						book.getEdition(), book.getYearPublished(),
						book.getCondition(), book.getPrice(),
						book.getComments(), book.getModDt(),
						book.getContactId() });

		return book;
	}

	public BookBean update(BookBean book) {
		book.setModDt(new Date());
		getJdbcTemplate().update(
				UPDATE_BOOK,
				new Object[] { book.getBookId(), book.getIsbn10(),
						book.getIsbn13(), book.getTitle(), book.getAuthor(),
						book.getEdition(), book.getYearPublished(),
						book.getCondition(), book.getPrice(),
						book.getComments(), book.getModDt(),
						book.getContactId() });

		return book;
	}

	public boolean remove(BookBean book) {
		try {
			getJdbcTemplate().update(REMOVE_BOOK,
					new Object[] { book.getBookId() });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public BookBean bookById(long id) {
		return (BookBean) getJdbcTemplate().queryForObject(SELECT_BY_ID,
				new Object[] { id }, new BookRowMapper());
	}

	public List<BookBean> search(String string) {
		return getJdbcTemplate().query(SELECT_BY_SEARCH,
				new Object[] { string }, new BookRowMapper());
	}

	public BookBean getLast() {
		return (BookBean) getJdbcTemplate().queryForObject(SELECT_LAST, new BookRowMapper());
	}

}
