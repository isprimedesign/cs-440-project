package com.isprime.albatross;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.isprime.albatross.beans.DepartmentBean;
import com.isprime.albatross.dao.DepartmentDaoImpl;

public class DepartmentTest {
	
	@Test
	public void test() {
		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
		
		DepartmentDaoImpl dao = (DepartmentDaoImpl)context.getBean("DepartmentDao");
		
		DepartmentBean dep1 = new DepartmentBean();
		dep1.setDepartmentCode("SO");
		dep1.setDepartmentDesc("Sociology");
		dep1.setModDt(new Date());
		
		//dao.add(dep1);
		
		DepartmentBean dep2 = new DepartmentBean();
		dep2.setDepartmentCode("SP");
		dep2.setDepartmentDesc("Spanish");
		dep2.setModDt(new Date());
		
		//dao.add(dep2);
		
		DepartmentBean dep3 = new DepartmentBean();
		dep3.setDepartmentCode("TR");
		dep3.setDepartmentDesc("Theatre");
		dep3.setModDt(new Date());
		
		//dao.add(dep3);
		
		DepartmentBean dep4 = new DepartmentBean();
		dep4.setDepartmentCode("TH");
		dep4.setDepartmentDesc("Theology");
		dep4.setModDt(new Date());
		
		//dao.add(dep4);
		
		List<DepartmentBean> list = dao.getAll();
		for(DepartmentBean d : list) {
			System.out.println(d.getDepartmentCode() + " - " + d.getDepartmentDesc());
		}
	}
}
