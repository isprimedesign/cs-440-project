package com.isprime.albatross;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.isprime.albatross.beans.BookBean;
import com.isprime.albatross.beans.BookToDepartmentBean;
import com.isprime.albatross.beans.ContactBean;
import com.isprime.albatross.dao.BookDaoImpl;
import com.isprime.albatross.dao.BookToDepartmentDaoImpl;
import com.isprime.albatross.dao.ContactDaoImpl;

public class TestPost {
//	@Test
//	public void test() {
//		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
//		ContactDaoImpl contactDao = (ContactDaoImpl)context.getBean("ContactDao");
//		BookDaoImpl bookDao = (BookDaoImpl)context.getBean("BookDao");
//		BookToDepartmentDaoImpl b2dDao = (BookToDepartmentDaoImpl)context.getBean("BooksToDepartmentDao");
//		
//		ContactBean contact = new ContactBean();
//		contact.setEmail("mrworksoutallthetime@devtoned.com");
//		contact.setName("Jonathon William Staff");
//		contact.setPhone("(812) 309-2554");
//		contact.setModDt(new Date());
//		
//		contactDao.add(contact);
//		ContactBean lastContact = contactDao.getLast();
//		Assert.assertEquals("Jonathon William Staff", lastContact.getName());
//		
//		BookBean book = new BookBean();
//		book.setIsbn10("0136147062");
//		book.setIsbn13("978-3-16-148410-0");
//		book.setTitle("Dude, where is my deodorant?");
//		book.setAuthor("Donald Alan Gardiner");
//		book.setEdition("Edition 1");
//		book.setYearPublished("2012");
//		book.setCondition("5");
//		book.setPrice("17$");
//		book.setComments("Seriously, where is my deodorant?");
//		book.setModDt(new Date());
//		book.setContactId(lastContact.getContactId());
//		
//		bookDao.add(book);
//		book.setBookId(bookDao.getLast().getBookId());
//		
//		BookToDepartmentBean b2d = new BookToDepartmentBean();
//		b2d.setBookId(book.getBookId());
//		b2d.setDepartmentCode("AC");
//		
//		b2dDao.addDepartmentToBook(b2d);
//	}
	
	@Test
	public void getBook() {
		ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Module.xml");
		BookDaoImpl bookDao = (BookDaoImpl)context.getBean("BookDao");
		
		List<BookBean> list = bookDao.search("Dude");
		
		for(BookBean b : list) {
			Assert.assertEquals("Seriously, where is my deodorant?", b.getTitle());
		}
	}
}
