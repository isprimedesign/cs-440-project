package com.isprime.albatross.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.isprime.albatross.beans.BookBean;
import com.isprime.albatross.beans.BookToDepartmentBean;
import com.isprime.albatross.beans.ContactBean;
import com.isprime.albatross.beans.DepartmentBean;
import com.isprime.albatross.dao.IBookDao;
import com.isprime.albatross.dao.IBookToDepartmentDao;
import com.isprime.albatross.dao.IContactDao;
import com.isprime.albatross.dao.IDepartmentDao;
import com.isprime.albatross.models.CreatePost;
import com.isprime.albatross.service.IPostService;

@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	@Inject
	private IPostService postService;

	@Inject
	private IContactDao contactService;
	@Inject
	private IBookDao bookService;
	@Inject
	private IBookToDepartmentDao b2dService;
	@Inject
	private IDepartmentDao departmentService;

	@RequestMapping(value = "/")
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);

		return "home";
	}

	@RequestMapping(value = "/post", method = RequestMethod.GET)
	public String viewPost(HttpServletRequest request, Locale locale,
			Model model) {
		String department = request.getParameter("department");
		model.addAttribute("department", department);

		long bookId = Long.parseLong(request.getParameter("postId"));
		BookBean book = bookService.bookById(bookId);

		// Post post = postService.postById(bookId);
		//
		// Right info
		model.addAttribute("bookTitle", book.getTitle());
		model.addAttribute("postDate", "December 12, 2012");
		model.addAttribute("bookPrice", book.getPrice());
		model.addAttribute("isbn10", book.getIsbn10());
		model.addAttribute("isbn13", book.getIsbn13());
		model.addAttribute("bookTitle", book.getTitle());
		model.addAttribute("authorNames", book.getAuthor());
		model.addAttribute("bookEdition", book.getEdition());
		model.addAttribute("yearPublished", book.getYearPublished());
		model.addAttribute("departments", department);

		// Left info
		model.addAttribute("bookCondition", book.getCondition());
		model.addAttribute("bookPrice", book.getPrice());
		model.addAttribute("bookComments", book.getComments());

		ContactBean contact = contactService.getById(book.getContactId());

		// Contact info
		model.addAttribute("contactName", contact.getName());
		model.addAttribute("contactEmail", contact.getEmail());
		model.addAttribute("contactPhone", contact.getPhone());

		return "/post";
	}

	@RequestMapping(value = "/booksInDepartment", method = RequestMethod.GET)
	public String viewBooksInDepartment(HttpServletRequest request,
			Locale locale, Model model) {
		String department = request.getParameter("department");

		DepartmentBean dep = departmentService.getDepartment(department);
		List<BookToDepartmentBean> b2dList = b2dService
				.getBooksFromDepartment(dep.getDepartmentCode());
		List<BookBean> bookList = new ArrayList<BookBean>();

		for (BookToDepartmentBean b : b2dList) {
			bookList.add(bookService.bookById(b.getBookId()));
		}

		model.addAttribute("postsList", bookList);
		model.addAttribute("department", department);

		return "/booksInDepartment";
	}

	@RequestMapping(value = "/departments")
	public String viewDepartments(Locale locale, Model model) {
		List<DepartmentBean> list = departmentService.getAll();

		model.addAttribute("departmentList", list);

		return "/departments";
	}

	@RequestMapping(value = "/create")
	public ModelAndView viewCreatePostPage(Locale locale, Model model) {
		List<DepartmentBean> list = departmentService.getAll();

		model.addAttribute("departmentsAvailable", list);

		return new ModelAndView("/create", "createPost", new CreatePost());
	}

	@RequestMapping(value = "/addPost", method = RequestMethod.POST)
	public String addContact(@ModelAttribute("createPost") CreatePost post,
			Model model) {
		logger.info("Shit going on! " + post.getDepartmentDesc());
		logger.info("Shit going on! " + post.getDepartmentCd());

		ContactBean contact = new ContactBean();
		contact.setEmail(post.getEmail());
		contact.setName(post.getName());
		contact.setPhone(post.getPhone());
		contact.setModDt(new Date());

		contactService.add(contact);
		contact.setContactId(contactService.getLast().getContactId());

		BookBean book = new BookBean();
		book.setIsbn10(post.getIsbn10());
		book.setIsbn13(post.getIsbn13());
		book.setTitle(post.getTitle());
		book.setAuthor(post.getAuthors());
		book.setEdition(post.getEdition());
		book.setYearPublished(post.getYearPublished());
		book.setCondition(post.getCondition());
		book.setPrice(post.getPrice());
		book.setComments(post.getComments());
		book.setModDt(new Date());
		book.setContactId(contact.getContactId());

		bookService.add(book);
		book.setBookId(bookService.getLast().getBookId());

		BookToDepartmentBean b2d = new BookToDepartmentBean();
		b2d.setDepartmentCode(post.getDepartmentCd());
		b2d.setBookId(book.getBookId());

		b2dService.addDepartmentToBook(b2d);

		// contactService.addContact(contact);
		// model.addAttribute(contactService.list());
		//
		String department = departmentService.getDepartment(
				post.getDepartmentCd()).getDepartmentDesc();
		logger.info("/post?department=" + department + "&postId="
				+ book.getBookId());

		return "redirect:post?department=" + department + "&postId="
				+ book.getBookId();
	}

	@RequestMapping(value = "/searchResults")
	public String viewSearchResults(Locale locale, Model model) {

		return "/searchResults";
	}

	public void setPostService(IPostService service) {
		this.postService = service;
	}

	public void setContactSerice(IContactDao service) {
		this.contactService = service;
	}

	public void setBookService(IBookDao service) {
		this.bookService = service;
	}

	public void setBookToDepartmentService(IBookToDepartmentDao service) {
		this.b2dService = service;
	}

	public void setDepartmentService(IDepartmentDao service) {
		this.departmentService = service;
	}

}