package com.isprime.albatross.service;

import java.util.List;

import com.isprime.albatross.models.Post;

public interface IPostService {
	public void addPost(Post post);
	public void removePost(Post post);
	public Post editPost(Post post);
	public List<Post> listAll();
	public List<Post> listBySearch(String searchString);
	public Post postById(long id);
	public long numberOfPosts(String department);
}
