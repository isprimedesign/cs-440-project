package com.isprime.albatross.service;

import java.util.ArrayList;
import java.util.List;

import com.isprime.albatross.models.Post;

public class PostServiceMock implements IPostService {
	private List<Post>posts; {
		posts = new ArrayList<Post>();
		
		Post post1 = new Post();
		post1.setPostDate("December 6, 2012");
		post1.setIsbn10("0136147062");
		post1.setIsbn13("978-3-16-148410-0");
		post1.setBookTitle("How to be a bad motherfucker");
		post1.setBookAuthor("Julius");
		post1.setBookEdition("Edition 1337");
		post1.setBookYear("1999");
		post1.setDepartment("Philosophy");
		post1.setCondition("5");
		post1.setPrice("$99.99");
		post1.setComment("Owned by a bad motherfucker");
		
		post1.setContactName("Jonathon William Staff");
		post1.setContactEmail("mrworksoutallthetime@devtoned.com");
		post1.setContactPhone("(812) 309-2554");
		
		posts.add(post1);
		
		Post post2 = new Post();
		post2.setPostDate("December 7, 2012");
		post2.setIsbn10("0136147062");
		post2.setIsbn13("978-3-16-148410-0");
		post2.setBookTitle("How to leave an uzi in the kitchen while taking a shit");
		post2.setBookAuthor("Vincent");
		post2.setBookEdition("Edition 1");
		post2.setBookYear("1999");
		post2.setDepartment("Theology");
		post2.setCondition("3");
		post2.setPrice("$14.99");
		post2.setComment("Not such a good idea");
		
		post2.setContactName("Paul Staff");
		post2.setContactEmail("mriknowallthewebend@devtoned.com");
		post2.setContactPhone("(812) 309-2966");
		
		posts.add(post2);
		
	}

	@Override
	public void addPost(Post post) {
		posts.add(post);
	}

	@Override
	public void removePost(Post post) {
		int count = 0;
		for(Post p : posts) {
			if(p.equals(post)) {
				posts.remove(count);
				break;
			}
			count++;
		}
	}

	@Override
	public Post editPost(Post post) {
		int count = 0;
		for(Post p : posts) {
			if(p.equals(post)) {
				posts.remove(count);
				
				p.setId(post.getId());
				p.setPostDate(post.getPostDate());
				p.setIsbn10(post.getIsbn10());
				p.setIsbn13(post.getIsbn13());
				p.setBookTitle(post.getBookTitle());
				p.setBookAuthor(post.getBookAuthor());
				p.setBookEdition(post.getBookEdition());
				p.setBookYear(post.getBookYear());
				p.setDepartment(post.getDepartment());
				p.setCondition(post.getCondition());
				p.setPrice(post.getPrice());
				p.setComment(post.getComment());
				
				p.setContactName(post.getContactName());
				p.setContactEmail(post.getContactEmail());
				p.setContactPhone(post.getContactPhone());
				
				posts.add(count, p);
				
				break;
			}
			count++;
		}
		
		return null;
	}

	@Override
	public List<Post> listAll() {
		return posts;
	}

	@Override
	public List<Post> listBySearch(String searchString) {
		List<Post> result = new ArrayList<Post>();
		
		for(Post p : posts) {
			if(p.getDepartment().toLowerCase().contains(searchString.toLowerCase())) {
				result.add(p);
			}
		}
		return result;
	}

	@Override
	public Post postById(long id) {
		for(Post p : posts) {
			if(p.getId() == id) {
				return p;
			}
		}
		return null;
	}

	@Override
	public long numberOfPosts(String department) {
		long counter = 0;
		
		for(Post p : posts) {
			if(p.getDepartment().toLowerCase().contains(department.toLowerCase())) {
				counter++;
			}
		}
		
		return counter;
	}
}
