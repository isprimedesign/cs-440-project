<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Albatross - Posting Page</title>

<link href="css/mainStyle.css" rel="stylesheet" type="text/css" />


</head>

<body>

	<div id="container">

		<jsp:include page="/partials/header.jsp"></jsp:include>

		<div id="content">

			<div id="pathLinks">
				<a href="../albatross-web/">Home</a> > <a href="departments.html">Categories</a>
			</div>

			<div id="browseContainer">
				<c:forEach items="${departmentList}" var="department">
					<a
						href="booksInDepartment.html?department=${department.departmentDesc}"
						class="browseCategoryItem"> <span class="categoryItemLarge">${department.departmentDesc}</span>
						<span class="categoryItemSmall">(1 books listed)</span>
					</a>
				</c:forEach>
			</div>

		</div>

		<jsp:include page="/partials/footer.jsp"></jsp:include>

	</div>


</body>
</html>