<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Albatross - Posting Page</title>

<link href="css/mainStyle.css" rel="stylesheet" type="text/css" />


</head>

<body>

	<div id="container">

		<jsp:include page="/partials/header.jsp"></jsp:include>

		<div id="content">

			<div class="contentSection">
				<div class="contentTitleText">Create a Listing</div>
				<div class="contentClear"></div>
			</div>

			<form:form method="post" action="addPost.html"
				modelAttribute="createPost">

				<div class="contentSection">
					<div class="contentTitle">Book Info</div>


					<div class="contentLeft">
						<div class="contentCreateLabel">ISBN 10</div>
						<div class="contentCreateInput">
							<form:input path="isbn10" />
						</div>
						<div class="contentCreateLabel">Title</div>
						<div class="contentCreateInput">
							<form:input path="title" />
						</div>
						<div class="contentCreateLabel">Edition</div>
						<div class="contentCreateInput">
							<form:input path="edition" />
						</div>
						<div class="contentCreateLabel">Department</div>
						<div class="contentCreateInput">
							<select name="departmentCd" tabindex="7">
								<c:forEach items="${departmentsAvailable}" var="departments">
									<option value="${departments.departmentCode}">${departments.departmentDesc}</option>
								</c:forEach>
							</select>
						</div>
						<div class="contentCreateLabel">Price</div>
						<div class="contentCreateInput">
							<form:input path="price" />
						</div>
					</div>

					<div class="contentRight">
						<div class="contentCreateLabel">ISBN 13</div>
						<div class="contentCreateInput">
							<form:input path="isbn13" />
						</div>
						<div class="contentCreateLabel">Authors</div>
						<div class="contentCreateInput">
							<form:input path="authors" />
						</div>
						<div class="contentCreateLabel">Year Published</div>
						<div class="contentCreateInput">
							<form:input path="yearPublished" />
						</div>
						<div class="contentCreateLabel">Condition</div>
						<div class="contentCreateInput">
							<select name="condition" tabindex="8">
								<option value="5">5/5</option>
								<option value="4">4/5</option>
								<option value="3">3/5</option>
								<option value="2">2/5</option>
								<option value="1">1/5</option>
							</select>
						</div>
					</div>

					<div class="contentClear"></div>

					<div class="contentLeft">
						<div class="contentCreateLabel">Comments</div>
						<div class="contentCreateInput">
							<textarea tabindex="10"></textarea>
						</div>
					</div>

					<div class="contentClear"></div>

				</div>


				<div class="contentSection">
					<div class="contentTitle">Contact Info</div>

					<div class="contentLeft">
						<div class="contentCreateLabel">Name</div>
						<div class="contentCreateInput">
							<form:input path="name" />
						</div>
						<div class="contentCreateLabel">Phone Number</div>
						<div class="contentCreateInput">
							<form:input path="phone" />
						</div>
					</div>

					<div class="contentRight">
						<div class="contentCreateLabel">Email Address</div>
						<div class="contentCreateInput">
							<form:input path="email" />
						</div>
					</div>

					<div class="contentClear"></div>

				</div>


				<div class="buttonSection">

					<input type="submit" value="Create Post" class="button" /> 
					<div class="contentClear"></div>

				</div>

			</form:form>
		</div>

		<jsp:include page="/partials/footer.jsp"></jsp:include>

	</div>


</body>
</html>
