<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Albatross - Posting Page</title>

<link href="css/mainStyle.css" rel="stylesheet" type="text/css" />


</head>

<body>

    <div id="container">

		<jsp:include page="/partials/header.jsp"></jsp:include>

        <div id="content">

            <div class="contentSection contentLinksDrilldown"><a href="../albatross-web/">Home</a> > <a href="departments.html">Departments</a> > <a href="booksInDepartment.html?department=${department}">${department}</a> > <a href="post.html">${bookTitle}</a></div>

            <div class="contentSection">
                <div class="contentTitleText">${bookTitle}</div>
                <div class="contentTitleSubText">${postDate} | ${bookPrice}</div>
                <div class="contentClear"></div>
            </div>

            <div class="contentSection">
                <div class="contentTitle">Book Info</div>
                
                <div class="contentLeft">
                    <div class="contentListItem"><strong>ISBN 10:</strong>  ${isbn10}</div>
                    <div class="contentListItem"><strong>ISBN 13:</strong>  ${isbn13}</div>
                    <div class="contentListItem"><strong>Title:</strong>  ${bookTitle}</div>
                    <div class="contentListItem"><strong>Author(s):</strong>  ${authorNames}</div>
                    <div class="contentListItem"><strong>Edition:</strong>  ${bookEdition}</div>
                    <div class="contentListItem"><strong>Year Published:</strong>  ${yearPublished}</div>
                    <div class="contentListItem"><strong>Department:</strong>  ${departments}</div>
                </div>

                <div class="contentRight">
                    <!-- eventually put an image here -->    
                    <div class="contentListItem"><strong>Condition:</strong>  ${bookCondition}</div>
                    <div class="contentListItem"><strong>Price:</strong>  ${bookPrice}</div>
                        <div class="contentListItem"><strong>Comments:</strong>  ${bookComments}</div>
                </div>
                
                <div class="contentClear"></div>
            </div>    

            <div class="contentSection">
                <div class="contentTitle">Contact Info</div>
                                
                <div class="contentLeft">
                        <div class="contentListItem"><strong>Name:</strong>  ${contactName}</div>
                    <div class="contentListItem"><strong>Email Adress:</strong>  ${contactEmail}</div>
                    <div class="contentListItem"><strong>Phone Number:</strong>  ${contactPhone}</div>
                </div>
                
                <div class="contentClear"></div>
            </div>

        </div>

		<jsp:include page="/partials/footer.jsp"></jsp:include>

    </div>

</body>
</html>