<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Project Albatross</title>

<link href="css/mainStyle.css" rel="stylesheet" type="text/css" />


</head>

<body>

    <div id="container">

		<jsp:include page="/partials/header.jsp"></jsp:include>

        <div id="content">

            <div id="searchContainer">Search for books:
                
                <input type="text" name="searchBarInput" />

                <a>
                    <img src="img/magnifyingGlass.png" height="24" width"24" />
                </a>

                <div style="clear:both"></div>

            </div>

            <div id="buttonHolder">

                <a href="departments.html" class="largeButton">

                    <img src="img/openBook.png">

                    <div class="largeButtonText">Browse</div>

                </a>

                <div class="inlinePadding" style="width: 15px"></div>

                <a href="create" class="largeButton">

                    <img src="img/add.png">

                    <div class="largeButtonText">Create Listing</div>

                </a>

            </div>

        </div>

		<jsp:include page="/partials/footer.jsp"></jsp:include>

    </div>

</body>
</html>